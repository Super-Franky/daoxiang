package com.cjs.dto;

import com.cjs.domain.Recruitment;
import lombok.Data;

@Data
public class RecruitmentDTO extends Recruitment {

    private Integer status = 0;

}
