package com.cjs.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.cjs.common.R;
import com.cjs.domain.Message;
import com.cjs.service.MessageService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("message")
public class MessageController {

    private final MessageService messageService;

    MessageController(MessageService messageService) {
        this.messageService = messageService;
    }

    /**
     * 分页获取全部个人信息
     */
    @GetMapping("list")
    public R<List<Message>> getAll(Integer userId) {
        List<Message> messages = messageService.list(Wrappers.<Message>lambdaQuery()
                .eq(Message::getUserId, userId)
                .orderByDesc(Message::getCreateTime));
        return R.success(messages);
    }

    /**
     * 获取对应用户未读信息
     */
    @GetMapping("getNotRead")
    public R<Integer> getNotRead(Integer userId) {
        Integer notRead = messageService.count(Wrappers.<Message>lambdaQuery()
                .eq(Message::getUserId, userId)
                .eq(Message::getHasRead, 0));
        return R.success(notRead);
    }

    /**
     * 更新信息,主要用于已读
     */
    @PutMapping
    public R<String> update(@RequestBody Message message) {
        messageService.updateById(message);
        return R.success("修改成功");
    }
}
