package com.cjs.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.cjs.common.R;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cjs.domain.ApplyStep;
import com.cjs.domain.Message;
import com.cjs.domain.Recruitment;
import com.cjs.domain.RecruitmentUser;
import com.cjs.dto.RecruitmentDTO;
import com.cjs.service.ApplyStepService;
import com.cjs.service.MessageService;
import com.cjs.service.RecruitmentService;
import com.cjs.service.RecruitmentUserService;
import org.springframework.beans.BeanUtils;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("recruitment")
public class RecruitmentController {

    private final RecruitmentService recruitmentService;

    private final RecruitmentUserService recruitmentUserService;

    private final MessageService messageService;

    private final ApplyStepService applyStepService;

    RecruitmentController(RecruitmentService recruitmentService, RecruitmentUserService recruitmentUserService,
                          MessageService messageService, ApplyStepService applyStepService) {
        this.recruitmentService = recruitmentService;
        this.recruitmentUserService = recruitmentUserService;
        this.messageService = messageService;
        this.applyStepService = applyStepService;
    }

    /**
     * 私有方法，用于判断字符串是否为空
     * @param s 输入的字符串
     * @return 字符串是否为空
     */
    private boolean stringIsNotNull(String s) {
        return s != null && !s.equals("");
    }

    /**
     * 分页获取全部招聘信息，可进行模糊搜索
     * @param page 页码
     * @param pageSize 每一页的数据量
     * @param userId 进行查询的用户id
     * @param search 搜索内容
     * @return 招聘信息
     */
    @GetMapping("page")
    public R<Page<RecruitmentDTO>> page(int page, int pageSize, Integer userId, String search,
                                        Integer type, Boolean ascByPrice, Integer lowPrice, Integer highPrice) {
        // 获取Recruitment的page
        Page<Recruitment> recruitmentPage = new Page<>(page, pageSize);
        recruitmentService.page(recruitmentPage, Wrappers.<Recruitment>lambdaQuery()
                .like(stringIsNotNull(search), Recruitment::getTitle, search)
                .eq(type != null, Recruitment::getType, type)
                .gt(lowPrice != null, Recruitment::getPrice, lowPrice)
                .le(highPrice != null, Recruitment::getPrice, highPrice)
                .orderBy(ascByPrice != null,
                        ascByPrice != null && ascByPrice, Recruitment::getPrice)
                .orderByDesc(Recruitment::getCreateTime));

        //将Recruitment的page转换为要返回的RecruitmentDTO的page
        Page<RecruitmentDTO> recruitmentDTOPage = new Page<>();
        //复制page信息
        BeanUtils.copyProperties(recruitmentPage, recruitmentDTOPage, "records");
        //复制page的record信息，同时将status加入RecruitmentDTO中
        List<Recruitment> recruitmentList = recruitmentPage.getRecords();
        List<RecruitmentDTO> recruitmentDTOS = recruitmentList.stream().map(recruitment -> {
            RecruitmentDTO recruitmentDTO = new RecruitmentDTO();
            BeanUtils.copyProperties(recruitment, recruitmentDTO);

            RecruitmentUser recruitmentUser = recruitmentUserService.getOne(Wrappers.<RecruitmentUser>lambdaQuery()
                    .eq(RecruitmentUser::getRecruitmentId, recruitment.getId())
                    .eq(RecruitmentUser::getUserId, userId)
                    .select(RecruitmentUser::getStatus));
            if (recruitmentUser != null)
                recruitmentDTO.setStatus(recruitmentUser.getStatus());
            else
                recruitmentDTO.setStatus(0);
            return recruitmentDTO;
        }).collect(Collectors.toList());
        recruitmentDTOPage.setRecords(recruitmentDTOS);
        return R.success(recruitmentDTOPage);
    }

    /**
     * 获取我的应聘记录，可进行模糊搜索
     * @param userId 进行查询的用户id
     * @param search 搜索内容
     * @return 应聘记录
     */
    @GetMapping("applies")
    public R<List<RecruitmentUser>> getApplies(Integer userId, String search,
                                         Integer type, Boolean ascByApplyTime) {
        // 获取我申请的职位
        List<RecruitmentUser> recruitmentUsers = recruitmentUserService.list(Wrappers.<RecruitmentUser>lambdaQuery()
                .eq(RecruitmentUser::getUserId, userId)
                .eq(RecruitmentUser::getStatus, 1)
                .like(stringIsNotNull(search), RecruitmentUser::getRecruitmentTitle, search)
                .eq(type != null, RecruitmentUser::getRecruitmentType, type)
                .orderBy(ascByApplyTime != null,
                        ascByApplyTime != null && ascByApplyTime, RecruitmentUser::getCreateTime)
        );
        return R.success(recruitmentUsers);
    }

    /**
     * 获取我的招聘记录，可进行模糊搜索
     * @param userId 进行查询的用户id
     * @param search 搜索内容
     * @return 招聘记录
     */
    @GetMapping("myRecruitmentsPage")
    public R<List<RecruitmentUser>> myRecruitmentPage(Integer userId, String search,
                                                      Integer type, Boolean ascByPostTime) {
        // 获取我申请的职位
        List<RecruitmentUser> recruitmentUsers = recruitmentUserService.list(Wrappers.<RecruitmentUser>lambdaQuery()
                .eq(RecruitmentUser::getUserId, userId)
                .eq(RecruitmentUser::getStatus, 2)
                .like(stringIsNotNull(search), RecruitmentUser::getRecruitmentTitle, search)
                .eq(type != null, RecruitmentUser::getRecruitmentType, type)
                .orderBy(ascByPostTime != null,
                        ascByPostTime != null && ascByPostTime, RecruitmentUser::getCreateTime)
        );
        return R.success(recruitmentUsers);
    }

    /**
     * 申请职位
     * @return 申请是否成功
     */
    @PostMapping("apply")
    public R<String> applyRecruitment(@RequestBody RecruitmentUser recruitmentUser) {
        // 获取对应的招聘
        Recruitment recruitment = recruitmentService.getById(recruitmentUser.getRecruitmentId());
        // 设置申请信息
        recruitmentUser.setRecruitmentTitle(recruitment.getTitle());
        recruitmentUser.setRecruitmentType(recruitment.getType());
        recruitmentUser.setStatus(1);
        recruitmentUser.setIsDelete(0);
        recruitmentUserService.save(recruitmentUser);
        // 设置申请信息
        messageService.applySuccessMessage(recruitmentUser.getUserId(), recruitmentUser);
        // 设置通知发布人
        RecruitmentUser recruiter = recruitmentUserService.getOne(Wrappers.<RecruitmentUser>lambdaQuery()
                .eq(RecruitmentUser::getRecruitmentId, recruitment.getId())
                .eq(RecruitmentUser::getStatus, 2));
        messageService.recruitmentAppliedMessage(recruiter.getUserId(), recruitmentUser);
        return R.success("申请成功");
    }

    /**
     * 发布招聘信息函数
     * @param recruitment 发布的招聘信息
     * @param userId 发布者
     * @return 是否创建成功
     */
    @PostMapping("post")
    public R<String> postRecruitment(@RequestBody Recruitment recruitment, Integer userId) {
        recruitmentService.save(recruitment);
        RecruitmentUser recruitmentUser = new RecruitmentUser();
        recruitmentUser.setRecruitmentId(recruitment.getId());
        recruitmentUser.setUserId(userId);
        recruitmentUser.setRecruitmentTitle(recruitment.getTitle());
        recruitmentUser.setRecruitmentType(recruitment.getType());
        recruitmentUser.setStatus(2);
        recruitmentUser.setIsDelete(0);
        recruitmentUserService.save(recruitmentUser);
        return R.success("发布招聘成功");
    }

    /**
     * 修改招聘信息
     * @param recruitment 招聘信息
     * @return 是否修改成功
     */
    @PutMapping
    public R<String> changeRecruitment(@RequestBody Recruitment recruitment) {
        recruitmentService.updateById(recruitment);
        recruitmentUserService.update(Wrappers.<RecruitmentUser>lambdaUpdate()
                .eq(RecruitmentUser::getRecruitmentId, recruitment.getId())
                .set(RecruitmentUser::getRecruitmentTitle, recruitment.getTitle()));
        return R.success("修改招聘信息成功");
    }

    /**
     * 取消招聘
     * @param id
     * @return
     */
    @DeleteMapping("{id}")
    public R<String> delete(@PathVariable Integer id) {
        // 删除原本
        recruitmentService.removeById(id);
        // 删除发布的记录
        recruitmentUserService.remove(Wrappers.<RecruitmentUser>lambdaQuery()
                .eq(RecruitmentUser::getRecruitmentId, id).eq(RecruitmentUser::getStatus, 2));
        // 将申请的记录改为已经删除
        recruitmentUserService.update(Wrappers.<RecruitmentUser>lambdaUpdate().
                eq(RecruitmentUser::getRecruitmentId, id)
                .set(RecruitmentUser::getIsDelete, 1));
        return R.success("删除成功");
    }

    /**
     * 删除申请记录
     * @return
     */
    @DeleteMapping("deleteApply")
    public R<String> deleteApply(@RequestBody RecruitmentUser recruitmentUser) {
        // 将申请的记录删除
        recruitmentUserService.removeById(recruitmentUser.getId());
        // 将申请的消息删除
        messageService.remove(Wrappers.<Message>lambdaQuery()
                .eq(Message::getRecruitmentUserId, recruitmentUser.getId())
                .eq(Message::getUserId, recruitmentUser.getUserId()));
        // 将消息删除
        applyStepService.remove(Wrappers.<ApplyStep>lambdaQuery()
                .eq(ApplyStep::getRecruitmentUserId, recruitmentUser.getId()));
        return R.success("删除成功");
    }

    /**
     * 根据id获取具体招聘信息
     * @param id
     * @param userId
     * @return
     */
    @GetMapping("{id}")
    public R<RecruitmentDTO> getById(@PathVariable Integer id, Integer userId) {
        Recruitment recruitment = recruitmentService.getById(id);
        RecruitmentUser recruitmentUser = recruitmentUserService.getOne(Wrappers.<RecruitmentUser>lambdaQuery()
                .eq(RecruitmentUser::getRecruitmentId, id)
                .eq(RecruitmentUser::getUserId, userId)
                .select(RecruitmentUser::getStatus));
        RecruitmentDTO recruitmentDTO = new RecruitmentDTO();
        BeanUtils.copyProperties(recruitment, recruitmentDTO);
        recruitmentDTO.setStatus(recruitmentUser.getStatus());
        return R.success(recruitmentDTO);
    }
}