package com.cjs.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.cjs.common.R;
import com.cjs.domain.User;
import com.cjs.service.UserService;
import org.apache.http.HttpEntity;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("user")
public class UserController {

    private final UserService userService;

    UserController(UserService userService) {
        this.userService = userService;
    }


    private String getUserInfo(String code) throws Exception {
        String url = "https://api.weixin.qq.com/sns/jscode2session";
        url += "?appid=wxd11a16c63685790a";//自己的appid
        url += "&secret=972ce44a618d281cb57fb96da1d21ee6";//自己的appSecret
        url += "&js_code=" + code;
        url += "&grant_type=authorization_code";
        url += "&connect_redirect=1";
        String res = null;
        CloseableHttpClient httpClient = HttpClientBuilder.create().build();
        HttpGet httpget = new HttpGet(url);    //GET方式
        CloseableHttpResponse response;
        // 配置信息
        RequestConfig requestConfig = RequestConfig.custom()          // 设置连接超时时间(单位毫秒)
                .setConnectTimeout(5000)                    // 设置请求超时时间(单位毫秒)
                .setConnectionRequestTimeout(5000)             // socket读写超时时间(单位毫秒)
                .setSocketTimeout(5000)                    // 设置是否允许重定向(默认为true)
                .setRedirectsEnabled(false).build();           // 将上面的配置信息 运用到这个Get请求里
        httpget.setConfig(requestConfig);                         // 由客户端执行(发送)Get请求
        response = httpClient.execute(httpget);                   // 从响应模型中获取响应实体
        HttpEntity responseEntity = response.getEntity();
        if (responseEntity != null) {
            res = EntityUtils.toString(responseEntity);
        }
        // 释放资源
        httpClient.close();
        response.close();
        JSONObject jo = JSON.parseObject(res);
        String openid = jo.getString("openid");
        System.out.println("openid" + openid);
        return openid;
    }

    /**
     * 根据openId登录，并将用户id返回给前端
     * @param code 前端获得的code
     * @return userId
     */
    @PostMapping("login")
    public R<Integer> login(String code) {
        String openId = null;
        try {
            openId = getUserInfo(code);
        } catch (Exception e) {
            e.printStackTrace();
        }
        User user = userService.getOne(Wrappers.<User>lambdaQuery().eq(User::getOpenId, openId));
        if (user == null) {
            user = new User();
            user.setAvatar("123");
            user.setOpenId(openId);
            user.setUsername("用户");
            user.setPhone("123456");
            user.setSex(0);
            userService.save(user);
        }
        return R.success(user.getId());
    }

    /**
     * 根据userId获取用户信息
     * @param id 用户id
     * @return 用户
     */
    @GetMapping("{id}")
    public R<User> getById(@PathVariable Integer id) {
        User user = userService.getById(id);
        return R.success(user);
    }

    @PutMapping
    public R<String> update(@RequestBody User user) {
        userService.updateById(user);
        return R.success("更改成功");
    }

}
