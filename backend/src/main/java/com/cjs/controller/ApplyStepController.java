package com.cjs.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.cjs.common.R;
import com.cjs.domain.ApplyStep;
import com.cjs.domain.Message;
import com.cjs.service.ApplyStepService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("applyStep")
public class ApplyStepController {

    private final ApplyStepService applyStepService;

    ApplyStepController(ApplyStepService applyStepService) {
        this.applyStepService = applyStepService;
    }

    /**
     * 获取步骤
     */
    @GetMapping("getSteps")
    public R<List<ApplyStep>> getSteps(Integer recruitmentId) {
        List<ApplyStep> applySteps = applyStepService.list(Wrappers.<ApplyStep>lambdaQuery()
                .eq(ApplyStep::getRecruitmentUserId, recruitmentId)
                .orderByDesc(ApplyStep::getCreateTime));
        return R.success(applySteps);
    }

}
