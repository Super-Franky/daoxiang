package com.cjs.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.cjs.common.R;
import com.cjs.domain.ApplyStep;
import com.cjs.domain.Message;
import com.cjs.domain.RecruitmentUser;
import com.cjs.service.ApplyStepService;
import com.cjs.service.MessageService;
import com.cjs.service.RecruitmentUserService;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("recruitmentUser")
public class RecruitmentUserController {

    private final RecruitmentUserService recruitmentUserService;

    private final MessageService messageService;

    private final ApplyStepService applyStepService;

    RecruitmentUserController(RecruitmentUserService recruitmentUserService, MessageService messageService,
                              ApplyStepService applyStepService) {
        this.recruitmentUserService = recruitmentUserService;
        this.messageService = messageService;
        this.applyStepService = applyStepService;
    }

    @GetMapping("{id}")
    public R<RecruitmentUser> getById(@PathVariable String id) {
        return R.success(recruitmentUserService.getById(id));
    }

    @PostMapping("passApply")
    public R<String> passApply(@RequestBody RecruitmentUser apply) {
        apply = recruitmentUserService.getById(apply.getId());
        messageService.applyPassMessage(apply);
        return R.success("通过成功");
    }

    @PostMapping("refuseApply")
    public R<String> refuseApply(@RequestBody RecruitmentUser apply) {
        apply = recruitmentUserService.getById(apply.getId());
        messageService.applyFailMessage(apply);
        return R.success("拒绝成功");
    }

    @GetMapping("getIsHandled/{id}")
    public R<Boolean> getIsHandled(@PathVariable String id) {
        int count = messageService.count(Wrappers.<Message>lambdaQuery()
                .eq(Message::getRecruitmentUserId, id)
                .ne(Message::getType, 1));
        return R.success(count > 1);
    }

    @DeleteMapping("{id}")
    public R<String> deleteById(@PathVariable String id) {
        recruitmentUserService.removeById(id);
        messageService.remove(Wrappers.<Message>lambdaQuery()
                .eq(Message::getRecruitmentUserId, id)
                .ne(Message::getType, 1));
        applyStepService.remove(Wrappers.<ApplyStep>lambdaQuery().eq(ApplyStep::getRecruitmentUserId, id));
        return R.success("删除成功");
    }
}
