package com.cjs.common;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class StringBooleanConverter implements Converter<String, Boolean> {
    @Override
    public Boolean convert(String s) {
        if (s.equals("true")) {
            return true;
        } else if (s.equals("false")) {
            return false;
        } else {
            return null;
        }
    }
}
