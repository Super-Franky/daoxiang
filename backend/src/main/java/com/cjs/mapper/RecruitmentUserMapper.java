package com.cjs.mapper;

import com.cjs.domain.RecruitmentUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author 0000
* @description 针对表【recruitment_user】的数据库操作Mapper
* @createDate 2022-05-21 15:59:05
* @Entity com.cjs.domain.RecruitmentUser
*/
public interface RecruitmentUserMapper extends BaseMapper<RecruitmentUser> {

}




