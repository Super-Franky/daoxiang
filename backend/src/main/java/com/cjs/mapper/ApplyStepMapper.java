package com.cjs.mapper;

import com.cjs.domain.ApplyStep;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author 0000
* @description 针对表【apply_step】的数据库操作Mapper
* @createDate 2022-05-30 10:23:58
* @Entity com.cjs.domain.ApplyStep
*/
public interface ApplyStepMapper extends BaseMapper<ApplyStep> {

}




