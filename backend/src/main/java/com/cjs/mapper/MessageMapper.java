package com.cjs.mapper;

import com.cjs.domain.Message;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author 0000
* @description 针对表【message】的数据库操作Mapper
* @createDate 2022-06-05 11:22:21
* @Entity com.cjs.domain.Message
*/
public interface MessageMapper extends BaseMapper<Message> {

}




