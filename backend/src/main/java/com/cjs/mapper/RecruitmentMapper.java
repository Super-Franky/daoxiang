package com.cjs.mapper;

import com.cjs.domain.Recruitment;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author 0000
* @description 针对表【recruitment】的数据库操作Mapper
* @createDate 2022-05-22 07:40:55
* @Entity com.cjs.domain.Recruitment
*/
public interface RecruitmentMapper extends BaseMapper<Recruitment> {

}




