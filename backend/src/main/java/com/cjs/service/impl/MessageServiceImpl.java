package com.cjs.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cjs.domain.ApplyStep;
import com.cjs.domain.Message;
import com.cjs.domain.RecruitmentUser;
import com.cjs.service.ApplyStepService;
import com.cjs.service.MessageService;
import com.cjs.mapper.MessageMapper;
import org.springframework.stereotype.Service;

import java.time.format.DateTimeFormatter;

/**
* @author 0000
* @description 针对表【message】的数据库操作Service实现
* @createDate 2022-06-05 11:22:21
*/
@Service
public class MessageServiceImpl extends ServiceImpl<MessageMapper, Message>
    implements MessageService{

    private ApplyStepService applyStepService;

    MessageServiceImpl(ApplyStepService applyStepService){
        this.applyStepService = applyStepService;
    }

    @Override
    public void applySuccessMessage(Integer userId, RecruitmentUser recruitmentUser) {
        // 申请信息
        Message message = new Message();
        message.setUserId(userId);
        message.setRecruitmentUserId(recruitmentUser.getId());
        message.setType(0);
        message.setTitle("申请进度");
        String description = "申请"+recruitmentUser.getRecruitmentTitle()+"成功，请等待处理";
        message.setDescription(description);
        this.save(message);
        // 申请步骤
        ApplyStep applyStep = new ApplyStep();
        applyStep.setRecruitmentUserId(recruitmentUser.getId());
        applyStep.setText("申请职位");
        String pattern = "yyyy-MM-dd HH:mm";
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(pattern);
        description = message.getCreateTime().format(formatter) + " 申请成功";
        applyStep.setDescription(description);
        applyStep.setType(0);
        applyStepService.save(applyStep);
    }

    @Override
    public void recruitmentAppliedMessage(Integer userId, RecruitmentUser recruitmentUser) {
        Message message = new Message();
        message.setUserId(userId);
        message.setRecruitmentUserId(recruitmentUser.getId());
        message.setType(1);
        message.setTitle("职位申请");
        String description = "有人申请您发布的职位，请点击处理。";
        message.setDescription(description);
        this.save(message);
    }

    @Override
    public void applyPassMessage(RecruitmentUser recruitmentUser) {
        // 信息
        Message message = new Message();
        message.setUserId(recruitmentUser.getUserId());
        message.setRecruitmentUserId(recruitmentUser.getId());
        message.setType(2);
        message.setTitle("申请进度");
        String description = "您的申请"+recruitmentUser.getRecruitmentTitle()+"已通过，点击查看";
        message.setDescription(description);
        this.save(message);
        // 申请步骤
        ApplyStep applyStep = new ApplyStep();
        applyStep.setRecruitmentUserId(recruitmentUser.getId());
        applyStep.setText("申请通过");
        String pattern = "yyyy-MM-dd HH:mm";
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(pattern);
        description = message.getCreateTime().format(formatter) + " 申请通过";
        applyStep.setDescription(description);
        applyStep.setType(0);
        applyStepService.save(applyStep);
    }

    @Override
    public void applyFailMessage(RecruitmentUser recruitmentUser) {
        // 信息
        Message message = new Message();
        message.setUserId(recruitmentUser.getUserId());
        message.setRecruitmentUserId(recruitmentUser.getId());
        message.setType(3);
        message.setTitle("申请进度");
        String description = "您的申请"+recruitmentUser.getRecruitmentTitle()+"未通过，点击查看";
        message.setDescription(description);
        this.save(message);
        // 申请步骤
        ApplyStep applyStep = new ApplyStep();
        applyStep.setRecruitmentUserId(recruitmentUser.getId());
        applyStep.setText("申请失败");
        String pattern = "yyyy-MM-dd HH:mm";
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(pattern);
        description = message.getCreateTime().format(formatter) + " 申请未通过";
        applyStep.setDescription(description);
        applyStep.setType(1);
        applyStepService.save(applyStep);
    }
}




