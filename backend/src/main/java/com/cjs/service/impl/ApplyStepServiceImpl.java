package com.cjs.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cjs.domain.ApplyStep;
import com.cjs.service.ApplyStepService;
import com.cjs.mapper.ApplyStepMapper;
import org.springframework.stereotype.Service;

/**
* @author 0000
* @description 针对表【apply_step】的数据库操作Service实现
* @createDate 2022-05-30 10:23:58
*/
@Service
public class ApplyStepServiceImpl extends ServiceImpl<ApplyStepMapper, ApplyStep>
    implements ApplyStepService{

}




