package com.cjs.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cjs.domain.Recruitment;
import com.cjs.service.RecruitmentService;
import com.cjs.mapper.RecruitmentMapper;
import org.springframework.stereotype.Service;

/**
* @author 0000
* @description 针对表【recruitment】的数据库操作Service实现
* @createDate 2022-05-22 07:40:55
*/
@Service
public class RecruitmentServiceImpl extends ServiceImpl<RecruitmentMapper, Recruitment>
    implements RecruitmentService{

}




