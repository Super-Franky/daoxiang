package com.cjs.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cjs.domain.User;
import com.cjs.service.UserService;
import com.cjs.mapper.UserMapper;
import org.springframework.stereotype.Service;

/**
* @author 0000
* @description 针对表【user】的数据库操作Service实现
* @createDate 2022-05-29 20:42:38
*/
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User>
    implements UserService{

}




