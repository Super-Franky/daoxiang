package com.cjs.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cjs.domain.RecruitmentUser;
import com.cjs.service.RecruitmentUserService;
import com.cjs.mapper.RecruitmentUserMapper;
import org.springframework.stereotype.Service;

/**
* @author 0000
* @description 针对表【recruitment_user】的数据库操作Service实现
* @createDate 2022-05-21 15:59:05
*/
@Service
public class RecruitmentUserServiceImpl extends ServiceImpl<RecruitmentUserMapper, RecruitmentUser>
    implements RecruitmentUserService{

}




