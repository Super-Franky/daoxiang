package com.cjs.service;

import com.cjs.domain.User;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author 0000
* @description 针对表【user】的数据库操作Service
* @createDate 2022-05-29 20:42:38
*/
public interface UserService extends IService<User> {

}
