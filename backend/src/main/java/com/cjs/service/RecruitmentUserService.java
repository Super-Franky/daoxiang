package com.cjs.service;

import com.cjs.domain.RecruitmentUser;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author 0000
* @description 针对表【recruitment_user】的数据库操作Service
* @createDate 2022-05-21 15:59:05
*/
public interface RecruitmentUserService extends IService<RecruitmentUser> {

}
