package com.cjs.service;

import com.cjs.domain.Message;
import com.baomidou.mybatisplus.extension.service.IService;
import com.cjs.domain.RecruitmentUser;

/**
* @author 0000
* @description 针对表【message】的数据库操作Service
* @createDate 2022-06-05 11:22:21
*/
public interface MessageService extends IService<Message> {
    public void applySuccessMessage(Integer userId, RecruitmentUser recruitmentUser);

    public void recruitmentAppliedMessage(Integer userId, RecruitmentUser recruitmentUser);

    public void applyPassMessage(RecruitmentUser recruitmentUser);

    public void applyFailMessage(RecruitmentUser recruitmentUser);
}
