package com.cjs.service;

import com.cjs.domain.ApplyStep;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author 0000
* @description 针对表【apply_step】的数据库操作Service
* @createDate 2022-05-30 10:23:58
*/
public interface ApplyStepService extends IService<ApplyStep> {

}
