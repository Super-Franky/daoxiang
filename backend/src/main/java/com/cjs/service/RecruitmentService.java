package com.cjs.service;

import com.cjs.domain.Recruitment;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author 0000
* @description 针对表【recruitment】的数据库操作Service
* @createDate 2022-05-22 07:40:55
*/
public interface RecruitmentService extends IService<Recruitment> {

}
