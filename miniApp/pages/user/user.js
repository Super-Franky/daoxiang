// pages/user/user.js
import Toast  from '../../miniprogram_npm/@vant/weapp/toast/toast';
import Dialog from '../../miniprogram_npm/@vant/weapp/dialog/dialog';
const app = getApp();
Page({
  data: {
    user: {},
    sexs: ["男", "女"],
    sex: 0,
    sexChoseShow: false,
    toBeRefesh: true,
    avatarUrl: ""
  },
  /**
   * 获取用户信息函数
   */
  getUser() {
      wx.request({
        url: app.globalData.baseUrl+"user/"+app.globalData.userId,
        method: "GET",
        success: (res) => {
          this.setData({
            user: res.data.data,
            sex: res.data.data.sex,
          });
          // 成功获取用户信息后再去获取用户的头像图片
          this.getavatar(res.data.data.avatar);
        },
      });
  },
  /**
   * 页面显示的生命周期，会根据toBeRefesh属性监听是否重新获取数据
   */
  onShow() {
    if (this.data.toBeRefesh) {
      this.getUser();
      this.setData({
        toBeRefesh: false,
      });
    };
  },
  /**
   * 点击更改用户名函数，将会跳转到更改用户名页面
   */
  onChangeUsername() {
    const username = JSON.stringify(this.data.user.username); 
    wx.navigateTo({
      url: `../changeUsername/changeUsername?username=${username}`,
    })
  },
    /**
   * 点击更改电话号码函数，将会跳转到更改电话号码页面
   */
  onChangePhone() {
    const phone = JSON.stringify(this.data.user.phone); 
    wx.navigateTo({
      url: `../changePhone/changePhone?phone=${phone}`,
    })
  },
  /**
   * 让选择性别选择器显示的函数
   */
  onSexChoseShow() {
    this.setData({
      sexChoseShow: true,
    })
  },
  /**
   * 关闭选择性别选择器的函数
   */
  onSexChoseClose() {
    this.setData({
      sexChoseShow: false,
    });
  },
  /**
   * 监听选择性别选择器改变的函数
   */
  onPickerChange(event) {
    this.setData({
      sex: event.detail.index,
    });
  },
  /**
   * 发送改变性别的请求的函数
   */
  onChangeSex() {
    Dialog.confirm({
      title: '确认更改？',
      confirmButtonColor: '#1E90FF' ,
    })
      .then(() => {
        wx.request({
          url: app.globalData.baseUrl+"user",
          method: "PUT",
          data: {
            id: app.globalData.userId,
            sex: this.data.sex,
          },
          success: (res) => {
            var user = this.data.user;
            user.sex = this.data.sex;
            this.setData({
              user: user,
              sexChoseShow: false,
            });
            Toast('更改成功');
          },
        });
      })
  },
  /**
   * 上传头像的函数
   */
  onClickAcater() {
    const that = this;
    wx.chooseMedia({
      count: 1,
      mediaType: ['image'],
      sourceType: ['album', 'camera'],
      camera: 'back',
      async success(res) {
        var fileUrl = 'user/' + app.uuid() + '.png';
        var tempFilePath = res.tempFiles[0].tempFilePath;
        //获取上传后的图片在云托管中的id的函数
        const result = await that.uploadFile(tempFilePath, fileUrl);
        //将获得的id设为数据库中的头像
        wx.request({
          url: app.globalData.baseUrl+"user",
          method: "PUT",
          data: {
            id: app.globalData.userId,
            avatar: result,
          },
          success: (res) => {
            Toast("更改成功");
            // 更改成功直接将本地的图片设为头像
            that.setData({
              avatarUrl: tempFilePath
            });
          },
        });
      }
    })
  },
  /**
   * 上传文件到云托管的函数
   * @param {*} file 文件本地路径
   * @param {*} path 文件在云端保存的路径
   * @param {*} onCall 回调函数
   */
  uploadFile(file, path, onCall = () => {}) {
    return new Promise((resolve, reject) => {
      const task = wx.cloud.uploadFile({
        cloudPath: path,
        filePath: file,
        config: {
          env: 'prod-2gzd0ip7962d3649' // 需要替换成自己的微信云托管环境ID
        },
        success: res => resolve(res.fileID),
        fail: e => {
          const info = e.toString()
          if (info.indexOf('abort') != -1) {
            reject(new Error('【文件上传失败】中断上传'))
          } else {
            reject(new Error('【文件上传失败】网络或其他错误'))
          }
        }
      })
      task.onProgressUpdate((res) => {
        if (onCall(res) == false) {
          task.abort()
        }
      })
    })
  },
  /**
   * 更具数据库中保存的头像在云端的id获取头像
   * @param {*} avatarId 
   */
  async getavatar(avatarId) {
    if (avatarId == '123') {
      this.setData({
        avatarUrl: 'https://img.yzcdn.cn/vant/cat.jpeg',
      });
    } else {
      const result = await this.getTempFile(avatarId,500);
      this.setData({
        avatarUrl: result.fileList[0].tempFileURL,
      });
    }
  },
  /**
   * 根据云端文件id获取文件的函数
   * @param {*} fileID 
   * @param {*} time 
   */
  async getTempFile(fileID, time = 86400) {
    const list = (typeof fileID === 'string' ? [fileID] : fileID).map(item => {
      return {
        fileID: item,
        maxAge: time,
      }
    });
    return await wx.cloud.getTempFileURL({
      fileList: list
    });
  }
})