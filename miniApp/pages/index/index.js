import Toast  from '../../miniprogram_npm/@vant/weapp/toast/toast';
const app = getApp();
Page({
  data: {
    search: "",
    recruitments: [],
    page: 1,
    options: [
      [
        { text: '全部招聘', value: null },
        { text: '个人招聘', value: 0 },
        { text: '企业招聘', value: 1 },
      ],
      [
        { text: '默认排序', value: null },
        { text: '薪资降序', value: false },
        { text: '薪资升序', value: true },
      ],
      [
        { text: '薪资不限', value: 0 },
        { text: '0-50',    value: 1 },
        { text: '50-100',  value: 2 },
        { text: '100-150', value: 3 },
        { text: '150以上', value: 4 },
      ]
    ],
    values: [null, null, 0],
    priceRange: [[null, null], [0, 50], [50, 100], [100, 150], [150, 1000000]]
  },
  /**
   * 获取招聘信息的函数，获取到的招聘信息会直接替换原有数据
   */
  requestRecruitments(data) {
    wx.request({
      url: app.globalData.baseUrl+"recruitment/page",
      method: "GET",
      data,
      success: (res) => {
        this.setData({
          recruitments: res.data.data.records,
          page: 2,
        });
      },
    });
  },
   /**
   * 获取更多招聘信息的函数，获取到的招聘信息会拼接到原有数据之后
   * 将在招聘信息拉到底层的时候触发
   */
  requestMoreRecruitments(data) {
    wx.request({
      url: app.globalData.baseUrl+"recruitment/page",
      method: "GET",
      data,
      success: (res) => {
        if (res.data.data.records.length > 0) {
          this.setData({
            recruitments: this.data.recruitments.concat(res.data.data.records),
            page: this.data.page + 1,
          });
        } else {
          Toast('没有更多招聘了');
        }
      },
    });
  },
  requestNotRead() {
    wx.request({
      url: app.globalData.baseUrl+"message/getNotRead",
      method: "GET",
      data: {
        userId: app.globalData.userId,
      },
      success: (res) => {
        if (res.data.data !== 0) {
          wx.setTabBarBadge({
            index: 3,
            text: String(res.data.data),
          });
        } else {
          wx.hideTabBarRedDot({
            index: 3
          });
        }
      },
    });
    setTimeout(() => { this.requestNotRead(); }, 1000);
  },
  /**
   * 页面加载
   */
  onLoad() {
    this.requestNotRead();
  },
  /**
   * 页面显示的函数
   */
  onShow() {
    const data = {
      page: 1,
      pageSize: 10,
      userId: app.globalData.userId,
    }
    this.requestRecruitments(data);
  },
  /**
   * 页面下拉拉触底事件的处理函数，如果现有数据大于0，会申请更多数据
   */
  onReachBottom() {
    if (this.data.recruitments.length > 0) {
      const values = this.data.values;
      const priceRange = this.data.priceRange[values[2]];
      const data = {
        page: this.data.page,
        pageSize: 10,
        userId: app.globalData.userId,
        type: values[0],
        ascByPrice: values[1],
        lowPrice: priceRange[0],
        highPrice: priceRange[1],
        search: this.data.search,
      }
      this.requestMoreRecruitments(data);
    }
  },
  /**
   * 接受搜索框内容的函数
   * @param {*} e 
   */
  onSearchChange(e) {
    this.setData({
      search: e.detail,
    });
  },
  /**
   * 搜索框进行搜索的函数
   */
  onSearch() {
    const values = this.data.values;
    const priceRange = this.data.priceRange[values[2]];
    const data = {
      page: 1,
      pageSize: 10,
      userId: app.globalData.userId,
      type: values[0],
      ascByPrice: values[1],
      lowPrice: priceRange[0],
      highPrice: priceRange[1],
      search: this.data.search,
    };
    this.requestRecruitments(data);
  },
  /**
   * 点击查看具体招聘的函数
   * @param {*} e 
   */
  onClickRecruitment(e) {
    const recruitment = JSON.stringify(e.currentTarget.dataset.orderid); 
    wx.navigateTo({
      url: `../recruitmentInfo/recruitmentInfo?recruitment=${recruitment}`
    });
  },
  /**
   * 筛选查询
   * @param {*} value 
   */
  onDropdownChange0(value) {
    var values = this.data.values;
    values[0] = value.detail;
    this.setData({
      values
    });
    this.onDropdownChange();
  },
  onDropdownChange1(value) {
    var values = this.data.values;
    values[1] = value.detail;
    this.setData({
      values
    });
    this.onDropdownChange();
  },
  onDropdownChange2(value) {
    var values = this.data.values;
    values[2] = value.detail;
    this.setData({
      values
    });
    this.onDropdownChange();
  },
  onDropdownChange() {
    const values = this.data.values;
    const priceRange = this.data.priceRange[values[2]];
    const data = {
      page: 1,
      pageSize: 10,
      userId: app.globalData.userId,
      type: values[0],
      ascByPrice: values[1],
      lowPrice: priceRange[0],
      highPrice: priceRange[1],
    }
    this.requestRecruitments(data);
  }
});