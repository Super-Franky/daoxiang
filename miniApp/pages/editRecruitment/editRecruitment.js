// pages/addRecruitment/addRecruitment.js
import Toast  from '../../miniprogram_npm/@vant/weapp/toast/toast';
import Dialog from '../../miniprogram_npm/@vant/weapp/dialog/dialog';
const app = getApp();
Page({
  data: {
      pageType: 0,
      id: 0,
      title: "",
      workplace: "",
      description: "",
      price: "",
      type: 0,
      newType: 0,
      phone: "",
      types: ["个人雇主", "企业雇主"],
      typeChoseShow: false,
      autosizeObject: {
        minHeight: 50
      }
  },
    /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    if (options.recruitment != undefined) {
      const recruitment = JSON.parse(options.recruitment);
      this.setData({
        pageType: recruitment.pageType,
        id: recruitment.id,
        title: recruitment.title,
        workplace: recruitment.workplace,
        description: recruitment.description,
        price: recruitment.price,
        type: recruitment.type,
        newType: recruitment.type,
        phone: recruitment.phone,
      });
    }
  },
  /**
   * 返回函数，返回上一层
   */
  onClickLeft() {
    wx.navigateBack({
      delta: 1,
    });
  },
  /**
   * 新增招聘函数，会校验数据的合理性。
   * 新增成功则返回上一层
   */
  onPostRecruitment() {
    var data = this.data;
    var numReg = /^[1-9]([0-9]+)?(.[0-9])?[0-9]*$/;
    var phoneReg= /^((0\d{2,3}-\d{7,8})|(1[34578]\d{9}))$/;  
    if (data.title == "" || data.workplace == "" || data.description == "" 
    || data.price == "" || data.phone == "") {
      Toast.fail("请完整填写");
      return;
    } else if (!numReg.test(data.price)) {
      Toast.fail("工资格式错误");
      return;
    } else if (!phoneReg.test(data.phone)) {
      Toast.fail("电话格式错误");
      return;
    } else if (data.price > 100000) {
      Toast.fail("工资过高");
      return;
    } 
    if (data.pageType === 0) {
      Dialog.confirm({
        title: '确认发布招聘？',
        confirmButtonColor: '#1E90FF' ,
      })
        .then(() => {
          wx.request({
            url: app.globalData.baseUrl+"recruitment/post?userId="+app.globalData.userId,
            method: "POST",
            data: {
              title: data.title,
              workplace: data.workplace,
              description: data.description,
              price: data.price,
              type: data.type,
              phone: data.phone
            },
            success: (res) => {
              Toast('发布成功');
              setTimeout(() => {
                wx.navigateBack({
                  delta: 1,
                });
              }, 1000);
            },
          });
        });
    } else {
      Dialog.confirm({
        title: '确认更新信息？',
        confirmButtonColor: '#1E90FF' ,
      })
        .then(() => {
          wx.request({
            url: app.globalData.baseUrl+"recruitment",
            method: "PUT",
            data: {
              id: data.id,
              title: data.title,
              workplace: data.workplace,
              description: data.description,
              price: data.price,
              type: data.type,
              phone: data.phone
            },
            success: (res) => {
              Toast('更新成功');
              setTimeout(() => {
                wx.navigateBack({
                  delta: 2,
                });
              }, 1000);
            },
          });
        });
    }
  },
  /**
   * 显示选择类型的选择框的函数
   */
  onTypeChoseShow() {
    this.setData({
      typeChoseShow: true,
    });
  },
  /**
   * 关闭选择类型的选择框的函数
   */
  onTypeChoseClose() {
    this.setData({
      typeChoseShow: false,
    });
  },
  /**
   * 选择类型的选择框改变了值的函数
   */
  onPickerChange(event) {
    this.setData({
      newType: event.detail.index,
    });
  },
  /**
   * 选择类型的选择框确认选择的函数
   */
  onChangeType() {
    this.setData({
      type: this.data.newType,
      typeChoseShow: false,
    });
  }
})