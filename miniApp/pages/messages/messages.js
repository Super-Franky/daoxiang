const app = getApp();
Page({
  data: {
    messages: []
  },
  requestMessages() {
    wx.request({
      url: app.globalData.baseUrl+"message/list",
      method: "GET",
      data: {
        userId: app.globalData.userId,
      },
      success: (res) => {
        let messages = res.data.data;
        for (let i = 0; i < messages.length; ++i) {
          switch (messages[i].type) {
            case 0: 
              messages[i].src = '/images/applyMessage.png';
              break;
            case 1:
              messages[i].src = '/images/recruitmentMessage.png';
              break;
            case 2: 
              messages[i].src = '/images/applySuccess.png';
              break;
            case 3: 
              messages[i].src = '/images/applyFail.png';
              break;
          }
        }
        this.setData({
          messages: messages
        })
      },
    });
    setTimeout(() =>  {
      this.requestMessages();
    }, 5000);
  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    this.requestMessages();
  },
  onMessageClick(e) {
    let message = e.currentTarget.dataset.orderid;
    if (message.hasRead === 0) {
      wx.request({
        url: app.globalData.baseUrl+"message",
        method: "PUT",
        data: {
          id: message.id,
          hasRead: 1,
        }
      })
    }
    if (message.type !== 1) {
      wx.request({
        url: app.globalData.baseUrl+`recruitmentUser/${message.recruitmentUserId}`,
        method: "GET",
        success: (res) => {
          const recruitment = res.data.data;
          const recruitmentJSON = JSON.stringify(recruitment); 
          wx.navigateTo({
            url: `../applyMessage/applyMessage?recruitment=${recruitmentJSON}`
          });
        }
      })
    } else {
      const applyId = message.recruitmentUserId;
      wx.request({
        url: app.globalData.baseUrl+`recruitmentUser/${applyId}`,
        method: "GET",
        success: (res) => {
          const recruitment = res.data.data;
          const recruitmentJSON = JSON.stringify(recruitment); 
          wx.navigateTo({
            url: `../recruitmentMessage/recruitmentMessage?recruitment=${recruitmentJSON}`
          });
        }
      })
    }
  },
})