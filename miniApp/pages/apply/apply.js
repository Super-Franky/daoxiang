import Dialog from '../../miniprogram_npm/@vant/weapp/dialog/dialog';
import Toast  from '../../miniprogram_npm/@vant/weapp/toast/toast';
const app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    search: '',
    appliedRecruitments: [],
    options: [
      [
        { text: '全部招聘', value: null },
        { text: '个人招聘', value: 0 },
        { text: '企业招聘', value: 1 },
      ],
      [
        { text: '默认排序', value: null },
        { text: '申请时间降序', value: true },
        { text: '申请时间升序', value: false },
      ]
    ],
    values: [null, null],
  },
  requestApplies(data) {
    wx.request({
      url: app.globalData.baseUrl+"recruitment/applies",
      method: "GET",
      data,
      success: (res) => {
        this.setData({
          appliedRecruitments: res.data.data,
        });
      },
    });
  },
  /**
   * 页面展示的生命周期
   */
  onShow() {
     const data = {
      userId: app.globalData.userId,
    };
    this.requestApplies(data);
  },
  /**
   * 接受搜索框内容的函数
   * @param {*} e 
   */
  onSearchChange(e) {
    this.setData({
      search: e.detail,
    });
  },
  /**
   * 搜索框进行搜索的函数
   */
  onSearch() {
    const values = this.data.values;
    const data = {
      userId: app.globalData.userId,
      type: values[0],
      ascByApplyTime: values[1],
      search: this.data.search
    }
    this.requestApplies(data);
  },
  /**
   * 点击了申请记录，跳转到申请职位详情的函数
   * @param {*} e 
   */
  onClickApply(e) {
    const recruitment = e.currentTarget.dataset.orderid;
    if (recruitment.isDelete === 1) {
      Dialog.confirm({
        title: '该招聘已被发布者删除',
        message: '是否删除申请记录？',
        confirmButtonColor: '#1E90FF' ,
      }).then(() => {
        wx.request({
          url: app.globalData.baseUrl+`recruitmentUser/${recruitment.id}`,
          method: "DELETE",
          success: (res) => {
            Toast(res.data.data);
            const data = {
              userId: app.globalData.userId,
            };
            this.requestApplies(data);
          },
        });
      })
    } else {
      const recruitmentJSON = JSON.stringify(recruitment); 
      wx.navigateTo({
        url: `../applyMessage/applyMessage?recruitment=${recruitmentJSON}`
      });
    }
  },
   /**
   * 筛选查询
   * @param {*} value 
   */
  onDropdownChange0(value) {
    var values = this.data.values;
    values[0] = value.detail;
    this.setData({
      values
    });
    this.onDropdownChange();
  },
  onDropdownChange1(value) {
    var values = this.data.values;
    values[1] = value.detail;
    this.setData({
      values
    });
    this.onDropdownChange();
  },
  onDropdownChange() {
    const values = this.data.values;
    const data = {
      userId: app.globalData.userId,
      type: values[0],
      ascByApplyTime: values[1],
    }
    this.requestApplies(data);
  }
})