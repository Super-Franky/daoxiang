import Dialog from '../../miniprogram_npm/@vant/weapp/dialog/dialog';
import Toast  from '../../miniprogram_npm/@vant/weapp/toast/toast';
const app = getApp();
Page({
  data: {
    recruitment: "",
  },
    /**
   * 返回函数，返回上一层
   */
  onClickLeft() {
    wx.navigateBack({
      delta: 0,
    });
  },
  /**
   * 生命周期函数--监听页面加载
   * 接收上一级传来的招聘详情数据
   */
  onLoad(options) {
    const recruitment = JSON.parse(options.recruitment);
    this.setData({
      recruitment: recruitment,
    });
  },
  /**
   * 点击申请职位
   */
  onApply() {
    Dialog.confirm({
      title: '确认申请该职位？',
      message: '你的个人信息将发送到招聘者手中',
      confirmButtonColor: '#1E90FF' ,
    })
      .then(() => {
        wx.request({
          url: app.globalData.baseUrl+"recruitment/apply",
          method: "POST",
          data: {
            recruitmentId: this.data.recruitment.id,
            userId: app.globalData.userId,
          },
          success: (res) => {
            Toast('申请成功');
            setTimeout(() => {
              wx.navigateBack({
                delta: 0,
              });
            }, 1000);
          },
        });
      })
  },
  /**
   * 点击编辑
   */
  onEdit() {
    const recruitment = this.data.recruitment;
    recruitment.pageType = 1;
    const recruitmentJSON = JSON.stringify(recruitment);
    wx.navigateTo({
      url: `../editRecruitment/editRecruitment?recruitment=${recruitmentJSON}`
    });
  },
  /**
   * 点击删除招聘
   */
  onDeleteRecruitment() {
    Dialog.confirm({
      title: '确认取消发布？',
      confirmButtonColor: '#1E90FF' ,
    })
      .then(() => {
        wx.request({
          url: app.globalData.baseUrl+`recruitment/${this.data.recruitment.id}`,
          method: "DELETE",
          success: (res) => {
            Toast('取消成功');
            setTimeout(() => {
              wx.navigateBack({
                delta: 0,
              });
            }, 1000);
          },
        });
      })
  },
  /**
   * 点击取消申请
   */
  onDeleteApply() {
    Dialog.confirm({
      title: '确认取消申请？',
      confirmButtonColor: '#1E90FF' ,
    })
      .then(() => {
        wx.request({
          url: app.globalData.baseUrl+'recruitment/deleteApply',
          method: "DELETE",
          data: {
            id: this.data.recruitment.id,
            userId: app.globalData.userId,
          },
          success: (res) => {
            Toast('取消成功');
            setTimeout(() => {
              wx.navigateBack({
                delta: 0,
              });
            }, 1000);
          },
        });
      })
  },
})