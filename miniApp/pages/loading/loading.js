import Toast  from '../../miniprogram_npm/@vant/weapp/toast/toast';
Page({
  data: {
    loadTime: 0,
  },
  /**
   * 第一次加载的函数，会调用loading方法
   * @param {*} options 
   */
  onLoad(options) {
    this.loading();
  },
  /**
   * 检查是否成功登录，若不成功登录，继续轮询检查
   * 若超过15秒仍未成功登录，则报错
   */
  loading() {
    if (getApp().globalData.userId == "") {
      if (this.data.loadTime < 15) {
        this.setData({
          loadTime: this.data.loadTime + 1,
        });
        setTimeout(() => this.loading(), 1000);
      } else {
        Toast('加载失败');
      }
    } else {
      wx.switchTab({
        url: '../index/index'
      });
    }
  },
})