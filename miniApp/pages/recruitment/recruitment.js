// pages/recruitment/recruitment.js
const app = getApp();
Page({
  data: {
    search: '',
    recruitments: [],
    options: [
      [
        { text: '全部招聘', value: null },
        { text: '个人招聘', value: 0 },
        { text: '企业招聘', value: 1 },
      ],
      [
        { text: '默认排序', value: null },
        { text: '发布时间降序', value: true },
        { text: '发布时间升序', value: false },
      ]
    ],
    values: [null, null],
  },
  requestRecruitments(data) {
    wx.request({
      url: app.globalData.baseUrl+"recruitment/myRecruitmentsPage",
      method: "GET",
      data,
      success: (res) => {
        this.setData({
          recruitments: res.data.data,
        });
      },
    });
  },
  /**
   * 页面显示的生命周期,每次显示都会重新申请数据
   */
  onShow() {
    const data = {
      userId: app.globalData.userId,
    };
    this.requestRecruitments(data);
  },
  /**
   * 接受搜索框内容的函数
   * @param {*} e 
   */
  onSearchChange(e) {
    this.setData({
      search: e.detail,
    });
  },
  /**
   * 搜索框进行搜索的函数
   */
  onSearch() {
    const values = this.data.values;
    const data = {
      userId: app.globalData.userId,
      type: values[0],
      ascByPostTime: values[1],
      search: this.data.search,
    }
    this.requestRecruitments(data);
  },
  /**
   * 点击查看招聘详情的函数
   * @param {*} e 
   */
  onClickRecruitment(e) {
    var recruitment = e.currentTarget.dataset.orderid;
    wx.request({
      url: app.globalData.baseUrl+`recruitment/${recruitment.recruitmentId}`,
      method: "GET",
      data: {
        userId: app.globalData.userId
      },
      success: (res) => {
        const recruitmentJSON = JSON.stringify(res.data.data); 
        wx.navigateTo({
          url: `../recruitmentInfo/recruitmentInfo?recruitment=${recruitmentJSON}`
        });
      },
    });
  },
  /**
   * 点击新增招聘的函数
   */
  onAddRecruitment() {
    wx.navigateTo({
      url: "../editRecruitment/editRecruitment"
    });
  },
     /**
   * 筛选查询
   * @param {*} value 
   */
  onDropdownChange0(value) {
    var values = this.data.values;
    values[0] = value.detail;
    this.setData({
      values
    });
    this.onDropdownChange();
  },
  onDropdownChange1(value) {
    var values = this.data.values;
    values[1] = value.detail;
    this.setData({
      values
    });
    this.onDropdownChange();
  },
  onDropdownChange() {
    const values = this.data.values;
    const data = {
      userId: app.globalData.userId,
      type: values[0],
      ascByPostTime: values[1],
    }
    this.requestRecruitments(data);
  }
})