import Toast  from '../../miniprogram_npm/@vant/weapp/toast/toast';
import Dialog from '../../miniprogram_npm/@vant/weapp/dialog/dialog';
const app = getApp();
Page({
  data: {
    phone: ""
  },
  /**
   * 第一次加载的生命周期，会接受上一级页面传来的电话号码参数
   * @param {*} options 
   */
  onLoad(options) {
    const phone = JSON.parse(options.phone);
    this.setData({
      phone: phone,
    });
  },
  /**
   * 返回上一级页面函数
   */
  onClickLeft() {
    wx.navigateBack({
      delta: 0,
    });
  },
  /**
   * 确认更改电话号码函数
   */
  onChangePhone() {
    var phoneReg= /^((0\d{2,3}-\d{7,8})|(1[34578]\d{9}))$/;  
    if (!phoneReg.test(this.data.phone)) {
      Toast.fail("电话格式错误");
      return;
    } 
    Dialog.confirm({
      title: '确认更改？',
      confirmButtonColor: '#1E90FF' ,
    })
      .then(() => {
        wx.request({
          url: app.globalData.baseUrl+"user",
          method: "PUT",
          data: {
            id: app.globalData.userId,
            phone: this.data.phone,
          },
          success: (res) => {
            let pages = getCurrentPages();
            let pageA = pages[pages.length - 2];
            pageA.setData({
              toBeRefesh: true,
            });
            Toast('更改成功');
            setTimeout(() => {
              wx.navigateBack({
                delta: 0,
              });
            }, 1000);
          },
        });
      })
  }
})