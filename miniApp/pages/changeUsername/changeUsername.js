
import Toast  from '../../miniprogram_npm/@vant/weapp/toast/toast';
import Dialog from '../../miniprogram_npm/@vant/weapp/dialog/dialog';
const app = getApp();
Page({
  data: {
    username: ""
  },
  /**
   * 第一次加载的生命周期，会接受上一级页面传来的用户名参数
   * @param {*} options 
   */
  onLoad(options) {
    const username = JSON.parse(options.username);
    this.setData({
      username: username,
    });
  },
  /**
   * 返回上一级页面函数
   */
  onClickLeft() {
    wx.navigateBack({
      delta: 0,
    });
  },
    /**
   * 确认更改用户名函数
   */
  onChangeUsername() {
    Dialog.confirm({
      title: '确认更改？',
      confirmButtonColor: '#1E90FF' ,
    })
      .then(() => {
        wx.request({
          url: app.globalData.baseUrl+"user",
          method: "PUT",
          data: {
            id: app.globalData.userId,
            username: this.data.username,
          },
          success: (res) => {
            let pages = getCurrentPages();
            let pageA = pages[pages.length - 2];
            pageA.setData({
              toBeRefesh: true,
            });
            Toast('更改成功');
            setTimeout(() => {
              wx.navigateBack({
                delta: 0,
              });
            }, 1000);
          },
        });
      })
  }
})