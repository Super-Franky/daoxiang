import Dialog from '../../miniprogram_npm/@vant/weapp/dialog/dialog';
import Toast  from '../../miniprogram_npm/@vant/weapp/toast/toast';
const app = getApp();
Page({
  data: {
    recruitment: "",
    steps: [],
    color: "green"
  },
  /**
   * 生命周期函数--监听页面加载
   * 接收上一级传来的招聘详情数据
   */
  onLoad(options) {
    const recruitment = JSON.parse(options.recruitment);
    this.setData({
      recruitment: recruitment,
    });
    wx.request({
      url: app.globalData.baseUrl+'applyStep/getSteps',
      method: "GET",
      data: {
        recruitmentId: recruitment.id,
      },
      success: (res) => {
        let steps = res.data.data;
        if (steps[0].type === 1) {
          steps[0].activeIcon = 'cross';
          this.setData({
            color: 'red'
          })
        }
        for (let i = 0; i < steps.length; ++i) {
          steps[i].desc = steps[i].description;
        }
        this.setData({
          steps: steps
        });
      },
    });
  },
  /**
   * 返回函数，返回上一层
   */
  onClickLeft() {
    wx.navigateBack({
      delta: 1,
    });
  },
    /**
   * 点击取消申请
   */
  onDeleteApply() {
    Dialog.confirm({
      title: '确认取消申请？',
      confirmButtonColor: '#1E90FF' ,
    })
      .then(() => {
        wx.request({
          url: app.globalData.baseUrl+'recruitment/deleteApply',
          method: "DELETE",
          data: {
            id: this.data.recruitment.id,
            userId: app.globalData.userId,
          },
          success: (res) => {
            Toast('取消成功');
            setTimeout(() => {
              wx.navigateBack({
                delta: 0,
              });
            }, 1000);
          },
        });
      })
  },
})