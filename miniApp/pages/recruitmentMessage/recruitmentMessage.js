import Toast  from '../../miniprogram_npm/@vant/weapp/toast/toast';
import Dialog from '../../miniprogram_npm/@vant/weapp/dialog/dialog';
const app = getApp();
Page({
  /**
   * 页面的初始数据
   */
  data: {
    applicant: "",
    recruitment: "",
    applyId: "",
    isHandled: true,
  },
    /**
   * 返回函数，返回上一层
   */
  onClickLeft() {
    wx.navigateBack({
      delta: 0,
    });
  },
  getIsHandled(recruitmentUserId) {
    wx.request({
      url: app.globalData.baseUrl+`recruitmentUser/getIsHandled/${recruitmentUserId}`,
      method: "GET",
      success: (res) => {
        this.setData({
          isHandled: res.data.data,
        })
      }
    })
  },
   /**
   * 获取招聘信息函数
   */
  getRecruitment(recruitmentId) {
    wx.request({
      url: app.globalData.baseUrl+`recruitment/${recruitmentId}`,
      method: "GET",
      data: {
        userId: app.globalData.userId
      },
      success: (res) => {
        let recruitment = res.data.data;
        recruitment.createTime = recruitment.createTime.replace('T', ' ');
        this.setData({
          recruitment: recruitment,
        })
      }
    })
  },
    /**
   * 获取用户信息函数
   */
  getUser(userId) {
    wx.request({
      url: app.globalData.baseUrl+`user/${userId}`,
      method: "GET",
      success: (res) => {
        this.setData({
          applicant: res.data.data,
        });
        // 成功获取用户信息后再去获取用户的头像图片
        this.getavatar(res.data.data.avatar);
      },
    });
  },
  /**
   * 更具数据库中保存的头像在云端的id获取头像
   * @param {*} avatarId 
   */
  async getavatar(avatarId) {
    let applicant = this.data.applicant;
    if (avatarId == '123') {
      applicant.avatar = 'https://img.yzcdn.cn/vant/cat.jpeg';
    } else {
      const result = await this.getTempFile(avatarId,500);
      applicant.avatar = result.fileList[0].tempFileURL;
    }
    this.setData({
      applicant: applicant,
    });
  },
    /**
   * 根据云端文件id获取文件的函数
   * @param {*} fileID 
   * @param {*} time 
   */
  async getTempFile(fileID, time = 86400) {
    const list = (typeof fileID === 'string' ? [fileID] : fileID).map(item => {
      return {
        fileID: item,
        maxAge: time,
      }
    });
    return await wx.cloud.getTempFileURL({
      fileList: list
    });
  },
 /**
   * 生命周期函数--监听页面加载
   * 接收上一级传来的招聘详情数据
   */
  onLoad(options) {
    const recruitment = JSON.parse(options.recruitment);
    this.setData({
      applyId: recruitment.id
    });
    this.getIsHandled(recruitment.id);
    this.getRecruitment(recruitment.recruitmentId);
    this.getUser(recruitment.userId);
  },
  /**
   * 通过申请
   */
  onPass() {
    if (this.data.isHandled)
      return;
    const that = this;
    Dialog.confirm({
      title: '确认通过申请？',
      confirmButtonColor: '#1E90FF' ,
    })
      .then(() => {
        wx.request({
          url: app.globalData.baseUrl+"recruitmentUser/passApply",
          method: "POST",
          data: {
            id: that.data.applyId,
          },
          success: (res) => {
            Toast(res.data.data);
            setTimeout(() => {
              wx.navigateBack({
                delta: 0,
              });
            }, 1000);
          },
        });
      })
  },
    /**
   * 拒绝申请
   */
  onRefuse() {
    if (this.data.isHandled)
      return;
    const that = this;
    Dialog.confirm({
      title: '确认拒绝申请？',
      confirmButtonColor: '#1E90FF' ,
    })
      .then(() => {
        wx.request({
          url: app.globalData.baseUrl+"recruitmentUser/refuseApply",
          method: "POST",
          data: {
            id: that.data.applyId,
          },
          success: (res) => {
            Toast(res.data.data);
            setTimeout(() => {
              wx.navigateBack({
                delta: 0,
              });
            }, 1000);
          },
        });
      })
  },
    /**
   * 点击查看具体招聘的函数
   * @param {*} e 
   */
  onClickRecruitment(e) {
    const recruitment = JSON.stringify(e.currentTarget.dataset.orderid); 
    wx.navigateTo({
      url: `../recruitmentInfo/recruitmentInfo?recruitment=${recruitment}`
    });
  },
})