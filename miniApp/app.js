// app.js
App({
  globalData: {
    // baseUrl: "http://101.42.233.51:8080/",
    // baseUrl: "http://localhost:8080/",
    baseUrl: "https://daoxiang-1899890-1311449665.ap-shanghai.run.tcloudbase.com/api/",
    userId: "",
  },
  onLaunch(options) {
    wx.cloud.init();
    var app = this;
    wx.login({
      success (res) {
        if (res.code) {
          wx.request({
            url: app.globalData.baseUrl + "user/login",
            method: "POST",
            data:{
              code: res.code,
            },
            header: {
              'content-type': 'application/x-www-form-urlencoded'
            },
            success: (res) => {
              app.globalData.userId = res.data.data;
            },
          })
        } else {
          console.log('登录失败！' + res.errMsg)
        }
      }
    });
  },
  /**
   * 用于生成uuid
   */
  uuid() {
    var s = [];
    var hexDigits = "0123456789abcdef";
    for (var i = 0; i < 36; i++) {
        s[i] = hexDigits.substr(Math.floor(Math.random() * 0x10), 1);
    }
    s[14] = "4"; // bits 12-15 of the time_hi_and_version field to 0010
    s[19] = hexDigits.substr((s[19] & 0x3) | 0x8, 1); // bits 6-7 of the clock_seq_hi_and_reserved to 01
    s[8] = s[13] = s[18] = s[23] = "-";

    var uuid = s.join("");
    return uuid;
  }
})
